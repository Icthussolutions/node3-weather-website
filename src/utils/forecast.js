const request = require('request');

const forecast = (latitude, longitude, callback) => {
    const url = 'https://api.darksky.net/forecast/d00b98d38033de314be95fbda9b114dc/' + encodeURIComponent(latitude) + ',' + encodeURIComponent(longitude)

    request({
        url,
        json: true
    }, (error, {
        body
    }) => {
        if (error) {
            callback('Unable to connect to weather service!');
        } else if (body.error) {
            callback('Unable to find location!');
        } else {
            callback(undefined,
                body.daily.data[0].summary + ' It is currently ' + body.currently.temperature + ' degrees outside. It feels like ' + body.currently.apparentTemperature + ' degrees. There is a ' + (body.currently.precipProbability * 100) + '% chance of rain.'
            );
        }
    });
};

module.exports = forecast